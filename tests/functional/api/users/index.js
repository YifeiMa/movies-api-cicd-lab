import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import User from "../../../../api/users/userModel";
import api from "../../../../index";

const expect = chai.expect;
let db;
let user1token;
let user1;

describe("Users endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });
  beforeEach(async () => {
    try {
      await User.deleteMany();
      // Register two users
      await request(api).post("/api/users?action=register").send({
        username: "user1",
        password: "test1"
      });
      await request(api).post("/api/users?action=register").send({
        username: "user2",
        password: "test2"
      });
      await request(api)
        .get("/api/users")
        .then((res) => {
          user1 = res.body[0];
        });
    } catch (err) {
      console.error(`failed to Load user test Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close();
  });
  describe("GET /api/users ", () => {
    it("should return the 2 users and a status 200", (done) => {
      request(api)
        .get("/api/users")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(2);
          let result = res.body.map((user) => user.username);
          expect(result).to.have.members(["user1", "user2"]);
          done();
        });
    });
  });

  describe("POST /api/users ", () => {
    describe("For a register action", () => {
      describe("when the payload is correct", () => {
        it("should return a 201 status and the confirmation message", () => {
          return request(api)
            .post("/api/users?action=register")
            .send({
              username: "user3",
              password: "test3"
            })
            .expect(201)
            .expect({ msg: "Successful created new user.", code: 201 });
        });
        after(() => {
          return request(api)
            .get("/api/users")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((res) => {
              expect(res.body.length).to.equal(3);
              const result = res.body.map((user) => user.username);
              expect(result).to.have.members(["user1", "user2", "user3"]);
            });
        });
      });
      describe("when the payload is not correct", () => {
        it("should return a 401 status and the confirmation message", () => {
          return request(api)
            .post("/api/users?action=register")
            .send({
              username: "user3"
            })
            .expect(401)
            .expect({
              success: false,
              msg: "Please pass username and password."
            });
        });
      });
    });
    describe("For an authenticate action", () => {
      describe("when the payload is correct", () => {
        it("should return a 200 status and a generated token", () => {
          return request(api)
            .post("/api/users?action=authenticate")
            .send({
              username: "user1",
              password: "test1"
            })
            .expect(200)
            .then((res) => {
              expect(res.body.success).to.be.true;
              expect(res.body.token).to.not.be.undefined;
              user1token = res.body.token.substring(7);
            });
        });
      });
      describe("when the user is correct but password not.", () => {
        it("should return a 401 status and the confirmation message", () => {
          return request(api)
            .post("/api/users?action=authenticate")
            .send({
              username: "user1",
              password: "test5"
            })
            .expect(401)
            .expect({
              msg: "Authentication failed. Wrong password.",
              code: 401
            });
        });
      });
      describe("when the user does not exist.", () => {
        it("should return a 401 status and the confirmation message", () => {
          return request(api)
            .post("/api/users?action=authenticate")
            .send({
              username: "yifei",
              password: "test5"
            })
            .expect(401)
            .expect({
              msg: "Authentication failed. User not found.",
              code: 401
            });
        });
      });
    });
  });
  describe("PUT /api/users/update/:id  ", () => {
    describe("when information is all true", () => {
      it("should update the user information and return a status 200", async () => {
        return request(api)
          .put(`/api/users/update/${user1._id}`)
          .send({
            username: user1.username,
            email: "20099867@wit.ie",
            password: "test4"
          })
          .set("Accept", "application/json")
          .set("Authentication", user1token)
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body.code).to.equal(200);
            expect(res.body.msg).to.be.a("string");
          });
      });
      after(() => {
        return request(api)
          .get("/api/users")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            const result = res.body[0];
            expect(result.email).to.equal("20099867@wit.ie");
          });
      });
    });
    describe("when user get wrong when update", () => {
      it("should return a status 404 and the confirmation message.", async () => {
        return request(api)
          .put(`/api/users/update/999`)
          .send({
            username: "user5",
            email: "20099867@wit.ie",
            password: "test4"
          })
          .set("Accept", "application/json")
          .expect(404)
          .expect({
            msg: "User not found",
            code: 404
          });
      });
    });
  });
  describe("Delete /api/users/delete/:id ", () => {
    describe("delete particularuser from user list", () => {
      it("should return a 200 status and the confirmation message", () => {
        return request(api)
          .delete(`/api/users/delete/${user1._id}`)
          .expect(200)
          .expect({ msg: "User Updated Sucessfully", code: 200 });
      });
      after(() => {
        return request(api)
          .get("/api/users")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body.length).to.equal(1);
            const result = res.body.map((user) => user.username);
            expect(result).to.have.members(["user2"]);
          });
      });
    });
    describe("delete unexist user from user list", () => {
      it("should return a 500 status and the confirmation message", () => {
        return request(api)
          .delete(`/api/users/delete/999`)
          .expect(500)
          .expect({ msg: "Cannot delete user", code: 500 });
      });
    });
  });

  describe("GET /api/users/:userName/favourites", () => {
    describe("when the username is valid", () => {
      it("should return the matching favourite movie information", () => {
        return request(api)
          .get(`/api/users/user1/favourites`)
          .set("Accept", "application/json")
          .expect(200)
          .then((res) => {
            expect(res.body.length).to.equal(0);
          });
      });
    });
    describe("when the username is invalid", () => {
      it("should return the 401 statue and the confirmation message.", () => {
        return request(api)
          .get(`/api/users/user123/favourites`)
          .set("Accept", "application/json")
          .expect(401)
          .expect({ code: 401, msg: "cannot get resource." });
      });
    });
  });

  describe("POST /api/users/:userName/favourites", () => {
    describe("when the username is valid", () => {
      it("should return a 201 status and post favourites id to the list", () => {
        return request(api)
          .post("/api/users/user1/favourites")
          .send({
            id: 671583
          })
          .expect(201)
          .then((res) => {
            expect(res.body.favourites.length).to.equal(1);
          });
      });
    });
    describe("when the username is invalid", () => {
      it("should return a 401 status and the confirmation message.", () => {
        return request(api)
          .post("/api/users/user123/favourites")
          .send({
            id: 671583
          })
          .expect(401)
          .expect({ code: 401, msg: "wrong username or id." });
      });
    });
    describe("when the movie ID is invalid", () => {
      it("should return a 401 status and the confirmation message.", () => {
        return request(api)
          .post("/api/users/user1/favourites")
          .send({
            id: 9999999
          })
          .expect(401)
          .expect({ code: 401, msg: "wrong username or id." });
      });
    });
  });
});
