import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Movie from "../../../../api/movies/movieModel";
import api from "../../../../index";
import movies from "../../../../seedData/movies";

const expect = chai.expect;
let db;
let user1token;

describe("Movies endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await Movie.deleteMany();
      await Movie.collection.insertMany(movies);
      await request(api)
        .post("/api/users?action=register")
        .send({ username: "user1", password: "test1" });
      await request(api)
        .post("/api/users")
        .send({ username: "user1", password: "test1" })
        .then((res) => {
          expect(res.body.success).to.be.true;
          expect(res.body.token).to.not.be.undefined;
          user1token = res.body.token;
        });
    } catch (err) {
      console.error(`failed to Load movie Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close(); // Release PORT 8080
  });
  describe("GET /api/movies ", () => {
    it("should return 20 movies and a status 200", () => {
      return request(api)
        .get("/api/movies")
        .set("Accept", "application/json")
        .set("Authorization", user1token)
        .expect(200)
        .then((res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(20);
        });
    });
    it("should return a status 401", () => {
      return request(api)
        .get("/api/movies")
        .set("Accept", "application/json")
        .expect(401)
        .then((res) => {
          expect(res.text).to.equal("Unauthorized");
        });
    });
  });

  describe("GET /api/movies/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching movie", () => {
        return request(api)
          .get(`/api/movies/${movies[0].id}`)
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("title", movies[0].title);
          });
      });
    });
    describe("when the id is valid but token is missing", () => {
      it("should return a states of 401", () => {
        return request(api)
          .get(`/api/movies/${movies[0].id}`)
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal("Unauthorized");
          });
      });
    });
    describe("when the id is invalid", () => {
      it("should return the 404 status and  NOT found message", () => {
        return request(api)
          .get("/api/movies/9999")
          .set("Accept", "application/json")
          .set("Authorization", user1token)
          .expect(404)
          .expect({
            status_code: 404,
            message: "The resource you requested could not be found."
          });
      });
    });

    describe("when the id is invalid and token is missing", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/9999")
          .set("Accept", "application/json")
          .expect(401)
          .then((res) => {
            expect(res.text).to.equal("Unauthorized");
          });
      });
    });
  });
});
