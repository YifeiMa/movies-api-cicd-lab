import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Trending from "../../../../api/trending/trendingModel";
import api from "../../../../index";
import trending from "../../../../seedData/trending";

const expect = chai.expect;
let db;

describe("Trending movies and TV series endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await Trending.deleteMany();
      await Trending.collection.insertMany(trending);
    } catch (err) {
      console.error(`failed to Load user Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close(); // Release PORT 8080
  });
  describe("GET /api/trending ", () => {
    it("should return 20 trending info and a status 200", (done) => {
      request(api)
        .get("/api/trending")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(20);
          done();
        });
    });
  });

  describe("GET /api/trending/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching trending movies and TV series", () => {
        return request(api)
          .get(`/api/trending/${trending[0].id}`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("title", trending[0].title);
          });
      });
    });
    describe("when the id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/trending/9999")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(404)
          .expect({
            status_code: 404,
            message: "The resource you requested could not be found."
          });
      });
    });
  });
});
