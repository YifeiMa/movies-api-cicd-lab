import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import People from "../../../../api/people/peopleModel";
import api from "../../../../index";
import people from "../../../../seedData/people";

const expect = chai.expect;
let db;

describe("Popular people endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await People.deleteMany();
      await People.collection.insertMany(people);
    } catch (err) {
      console.error(`failed to Load people Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close(); // Release PORT 8080
  });
  describe("GET /api/people ", () => {
    it("should return 20 people info and a status 200", (done) => {
      request(api)
        .get("/api/people")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(20);
          done();
        });
    });
  });

  describe("GET /api/people/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching popular people", () => {
        return request(api)
          .get(`/api/people/${people[0].id}`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("name", people[0].name);
          });
      });
    });
    describe("when the id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/people/9999")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(404)
          .expect({
            status_code: 404,
            message: "The resource you requested could not be found."
          });
      });
    });
  });
});
