# Assignment 2 - Agile Software Practice.

Name: Yifei Ma

## API endpoints.

[List the Web API's endpoints and state the purpose of each one. Indicate those that require authentication.]

- /api/users | GET | Gets a list of users
- /api/users | POST | login as a particular user
- /api/users?action=register | POST | add a new user
- /api/users/update/${id} | PUT | modify information of exist user.
- /api/users/delete/${id} | DELETE | delete exist user from user list.
- /api/users/:userName/favourites | GET | get a list of favourite movies(tv series) ID of one user.
- /api/users/:userName/favourites | PUT | add a favourite movie/series to favourite list.
- /api/series | GET | Gets a list of TV series
- /api/series/{id} | GET | Gets a single TV series
- /api/people | GET | Gets a list of popular actors
- /api/trending | GET | Gets a list of trending movies/TV series

#### require authentication

- /api/movies | GET | Gets a list of movies
- /api/movies/{movieid} | GET | Gets a single movie

## Test cases.

[In this section, include a listing of the response from running your tests locally (npm run test). Simply copy the output from your terminal and paste it into a fenced block (the triple tilda markup, i.e. ~~~ ), as shown below - do not use a screenshot.]

```
  Users endpoint
    GET /api/users
      √ should return the 2 users and a status 200
    POST /api/users
      For a register action
        when the payload is correct
          √ should return a 201 status and the confirmation message (279ms)
        when the payload is not correct
          √ should return a 401 status and the confirmation message
      For an authenticate action
        when the payload is correct
          √ should return a 200 status and a generated token (307ms)
        when the user is correct but password not.
          √ should return a 401 status and the confirmation message (293ms)
        when the user does not exist.
          √ should return a 401 status and the confirmation message
    PUT /api/users/update/:id
      when information is all true
        √ should update the user information and return a status 200 (319ms)
      when user get wrong when update
        √ should return a status 404 and the confirmation message.
    Delete /api/users/delete/:id
      delete particularuser from user list
        √ should return a 200 status and the confirmation message
      delete unexist user from user list
        √ should return a 500 status and the confirmation message
    GET /api/users/:userName/favourites
      when the username is valid
        √ should return the matching favourite movie information (53ms)
      when the username is invalid
        √ should return the 401 statue and the confirmation message. (69ms)
    POST /api/users/:userName/favourites
      when the username is valid
        √ should return a 201 status and post favourites id to the list (100ms)
      when the username is invalid
        √ should return a 401 status and the confirmation message. (53ms)
      when the movie ID is invalid
        √ should return a 401 status and the confirmation message. (54ms)

  Movies endpoint
    GET /api/movies
      √ should return 20 movies and a status 200 (63ms)
      √ should return a status 401
    GET /api/movies/:id
      when the id is valid
        √ should return the matching movie (49ms)
      when the id is valid but token is missing
        √ should return a states of 401
      when the id is invalid
        √ should return the 404 status and  NOT found message (54ms)
      when the id is invalid and token is missing
        √ should return the NOT found message

  Trending movies and TV series endpoint
    GET /api/trending
      √ should return 20 trending info and a status 200 (41ms)
    GET /api/trending/:id
      when the id is valid
        √ should return the matching trending movies and TV series
      when the id is invalid
        √ should return the NOT found message

  Popular people endpoint
    GET /api/people
      √ should return 20 people info and a status 200 (90ms)
    GET /api/people/:id
      when the id is valid
        √ should return the matching popular people
      when the id is invalid
        √ should return the NOT found message (47ms)


  27 passing (20s)
```

## heroku

only several basic API.

- [trending](https://movies-api-production-yifei.herokuapp.com/api/trending)
- [user](https://movies-api-production-yifei.herokuapp.com/api/users)
- [movies](https://movies-api-production-yifei.herokuapp.com/api/movies)(protected)
- [genres](https://movies-api-production-yifei.herokuapp.com/api/genres)
- [people](https://movies-api-production-yifei.herokuapp.com/api/people)

## Independent Learning (if relevant)

For Option B, [the URL of the Coveralls webpage](https://coveralls.io/builds/55648188)
